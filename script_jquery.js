//$(seletor).ação()
$(document).ready(function() { //verificar se o documento carregou
  $('#btn_esconder').click(function(){
    $('#hide_h1').hide();
    $("hide_h1").delay(1000); //pausa a execução
    $("#hide_h1").fadeIn(3000); // Reaparece o objeto ID hide_h1
  });
});


$(document).ready(function(){
  $("#azul").click(function(){ //Usando o ID do objeto com o '#'
    $("p").css({"color": "blue", "background-color": "yellow"});// Alterando o CSS
    $("#mensagem")//encadeamento com identação (deixar o ; somente na ultima linha)
      .show()
      .text("Cor alterada com sucesso!")//exibe uma mensagem na tag "span"
      .css({"border": "1px solid red"}) // dessa maneira o bg-color deve ser escrito em camelCase backgroundColor
      .delay(2000)
      .fadeOut("fast")
      .addClass("green"); //adiciona a classe ao span
  });
  $("#verde").click(function(){
    $("p")
      .css({"color": "green", "background-color": "black"}) //agrupamento feito usando chaves "{}"
      .fadeOut("slow")
      .delay(1000)
      .fadeIn("slow");
    $("#verde").removeClass("red"); //remove a classe do button
  });
});
