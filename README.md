# Curso relampago javascript e jquery

__Bora aprender para fazer umas paginas legaizinhas :)__

- Variaveis
- If/Else
- While
- Tipos de dados / listas
- Funções
- Eventos
- DOM
- Validando formulário

__Agora começa o JQuery__

- Como baixa-lo
- Sintaxe do jquery
- Alternando o CSS e especificando com ID
- Efeito Fade IN/OUT
- Inserindo texto no html
- Encadeamento e agrupamento de codigo
- Adicionando e removendo Classes
- Criando carrossel com JQuery
