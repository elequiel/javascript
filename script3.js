// 1 jeito de chamar a função
function soma(a, b){
  console.log(a+b); //retorna na guia console.
}
soma(5, 5)

//outro jeito de chamar a função
function sub(a, b){
  return a-b;
}
var s = sub(5, 3)
console.log(s)


//terceiro jeito de chamar a função
function mul(a, b){
  return a*b;
}
console.log(mul(4, 5));
